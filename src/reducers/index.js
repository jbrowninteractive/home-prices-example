
import {combineReducers} from 'redux'
import Consts from 'consts'

export default combineReducers(
{
    hpi : (state={}, action) =>
    (
        action.type === Consts.actions.SET_HPI
            ? action.payload
            : state
    )
})
