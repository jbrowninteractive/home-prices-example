
export default
{

  actions:
  {
    SET_HPI: 'setHpi'
  },
  colors:
  {
    BLACK : '#000',
    WHITE : '#FFF',
    ORANGE: '#f27b35',
    GREEN: '#38b506',
    BLUE: '#35a2f2',
    DARK_GREY: '#1b1b1b',
    DARK_GREY2: '#212121',
    LITE_GREY: '#eaeaea'
  },
  LOAD_DELAY: 0
}
