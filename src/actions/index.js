
import Consts from 'consts'
import hpi from 'modules/data/shiller.json'
import {sleep} from 'modules/utils'


export const getHpi = () => async (dispatch) =>
{
  await sleep(Consts.LOAD_DELAY)
  dispatch({type: Consts.actions.SET_HPI, payload: hpi})
  return hpi
}

export const setHpi = (value) =>
({
    type: Consts.actions.SET_HPI,
    payload: value
})
