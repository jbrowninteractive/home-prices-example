
import React, {Component} from 'react'
import Graph from 'components/ui/graph'
import './index.css'

export default class Home extends Component
{
  render = () =>
    <div className='screen-home'>
      <Graph />
    </div>

  static defaultProps =
  {
    hpi: []
  }
}
