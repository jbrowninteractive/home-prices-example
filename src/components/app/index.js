
import React, {Component} from 'react'
import connect from './connect'
import Loading from 'components/ui/loading'
import Header from 'components/ui/header'
import Home from 'components/screens/home'
import './index.css'

export default connect( class App extends Component
{
  componentWillMount = () =>
  {
    this.props.getHpi()
  }

  render = () =>
    <div className='app'>
      <Header />
      {this.props.loading
        ? <Loading />
        : <Home />
      }
    </div>

  static defaultProps =
  {
    loading: true
  }
})
