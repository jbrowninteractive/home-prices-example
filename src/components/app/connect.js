
import {connect} from 'react-redux'
import {getHpi} from 'actions'


const mapStateToProps = (props) =>
({
  loading: !props.hpi
})

const mapDispatchToProps = (dispatch) =>
({
  getHpi()
  {
    dispatch( getHpi() )
  }
})

export default (Component) => connect(mapStateToProps, mapDispatchToProps)(Component)
