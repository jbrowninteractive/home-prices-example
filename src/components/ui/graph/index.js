import React, {Component} from 'react'
import GraphLabel from 'components/ui/graph-label'
import GraphBackground from 'components/ui/graph-background'
import GraphMarkers from 'components/ui/graph-markers'
import GraphLine from 'components/ui/graph-line'
import GraphModal from 'components/ui/graph-modal'
import Consts from "consts"
import connect from './connect'
import './index.css'

export default connect( class Header extends Component
{
  indexMarkers =
    Array.from(Array(201).keys()).reverse()
      .filter((value) => value%20===0)

  populationMarkers =
    Array.from(Array(360).keys()).reverse()
      .filter((value) => value>30 && value%30===0)

  yearMarkers =
    Array.from(Array(2030).keys())
      .filter((value) => value>1880 && value%10===0)


  render = () =>
    <div className='ui-graph'>
      <div className='top-container'>
        <GraphLabel className='label-indexes'
          value='Case-Shiller Index' vertical={true} />
        <GraphMarkers className='markers-indexes' vertical={true}
          side={'left'} values={this.indexMarkers} />
        <GraphBackground horizontalRules={10} verticalRules={10}>
          <GraphLine
            xValues={this.props.years}
            yValues={this.props.indexes}
            minXValue={1890}
            maxXValue={2020}
            minYValue={0}
            maxYValue={200}
            color={Consts.colors.GREEN}
            />
          <GraphLine
            xValues={this.props.years}
            yValues={this.props.populations}
            minXValue={1890}
            maxXValue={2020}
            minYValue={60}
            maxYValue={330}
            color={Consts.colors.BLUE}
            />
          <GraphModal
            indexes={this.props.indexes}
            years={this.props.years}
            populations={this.props.populations}
            />
        </GraphBackground>
        <GraphMarkers className='markers-populations' vertical={true}
          side={'right'} values={this.populationMarkers} />
        <GraphLabel className='label-populations'
          value='Population (millions)' vertical={true} />
      </div>
      <div className='bottom-container'>
        <GraphMarkers values={this.yearMarkers} />
        <GraphLabel className='label-years' value='Year' vertical={false} />
      </div>
    </div>

  static defaultProps =
  {
    indexes: [],
    populations: [],
    years: []
  }
})
