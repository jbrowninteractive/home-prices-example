
import {connect} from 'react-redux'


const mapStateToProps = (props) =>
({
  hpi: props.hpi,
  indexes: props.hpi.map((item) => item.index),
  populations: props.hpi.map((item) => item.population),
  years: props.hpi.map((item) => item.year)
})

const mapDispatchToProps = (dispatch) =>
({
})

export default (Component) => connect(mapStateToProps, mapDispatchToProps)(Component)
