
import React, {Component} from 'react'
import './index.css'

export default class GraphBackground extends Component
{

  className =
    `ui-graph-background ${this.props.className}`

  getRules(times, direction)
  {
    let nodes = []

    for(var i=0; i<times; i++)
    {
      nodes.push(
        <div
        key={`rule-${direction}-${i}`}
        className={`rule ${direction}`}
        />
      )
    }

    return nodes
  }

  render = () =>
    <div className={this.className}>
      <div className='rule-container horizontal'>
        {this.getRules(this.props.horizontalRules, 'horizontal')}
      </div>
      <div className='rule-container vertical'>
        {this.getRules(this.props.verticalRules, 'vertical')}
      </div>
      <div className='children'>
        {this.props.children}
      </div>
    </div>

  static defaultProps =
  {
    className: '',
    horizontalRules: 8,
    verticalRules: 8
  }
}
