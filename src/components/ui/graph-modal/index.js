import React, {Component} from 'react'
import './index.css'

export default class GraphModal extends Component
{

  className =
    `ui-graph-modal ${this.props.className}`

  duration = 500

  drag = 10

  state =
  {
    x: 0,
    y: 0,
    a: 0,
    index: 0,
    population: 0,
    year: 0
  }

  get containerStyle()
  {
    return ({
        left: this.state.x,
        top: this.state.y,
        opacity: this.state.a
    })
  }

  getItem = (array, ratio) =>
  {
    return array[Math.floor((array.length-1) * ratio)]
  }

  onMouseMove = (event) =>
  {
    let rect = event.target.getBoundingClientRect()
    let layerX = event.clientX - rect.x
    let layerY = event.clientY - rect.y
    let xRatio = layerX / rect.width
    let i = Math.floor((this.props.years.length-1) * xRatio)

    if(i < 0)
    {
      i = 0
    }
    else if(i > this.props.years.length-1)
    {
      i = this.props.years.length-1
    }

    let year = this.props.years[i]
    let index = this.props.indexes[i]
    let population = this.props.populations[i]

    this.setState({x:layerX, y:layerY, a: 1,
      index: index, population: population, year: year})
  }

  onMouseLeave = () =>
  {
    this.setState({a:0})
  }

  render = () =>
    <div
      className={this.className}
      onMouseMove={this.onMouseMove}
      onMouseLeave={this.onMouseLeave}
      >
      <div
        className="container"
        style={this.containerStyle}
        >
        <div className="info indexes">
          <div className="label">Index:</div>
          <div className="value">{this.state.index}</div>
        </div>
        <div className="info population">
          <div className="label">Population:</div>
          <div className="value">{this.state.population}</div>
        </div>
        <div className="info year">
          <div className="label">Year:</div>
          <div className="value">{this.state.year}</div>
        </div>
      </div>
    </div>

  static defaultProps =
  {
    className: ""
  }
}
