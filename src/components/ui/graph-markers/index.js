import React, {Component} from 'react'
import './index.css'

export default class GraphMarkers extends Component
{
  className =
    `ui-graph-markers ${this.props.vertical ? ' vertical' : ''} ${this.props.side} ${this.props.className}`

  markerClassName =
    `marker ${this.props.vertical ? ' vertical' : ''}`

  render = () =>
    <div className={this.className}>
      {this.props.values.map((value, i) =>
        <div key={i} className={this.markerClassName}>{value}</div>)}
    </div>

  static defaultProps =
  {
    className: '',
    values: [],
    vertical: false,
    side: 'left'
  }
}
