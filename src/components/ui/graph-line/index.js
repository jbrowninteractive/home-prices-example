
import React, {Component} from 'react'
import Dimensions from 'react-dimensions'
import './index.css'

export default Dimensions()( class GraphLine extends Component
{
  constructor(props)
  {
    super(props)
    this.componentDidMount = this.updateCanvas
    this.componentDidUpdate = this.updateCanvas
  }

  className =
    `ui-graph-line ${this.props.className}`

  canvas =
    React.createRef()

  updateCanvas = () =>
  {
    let cvs = this.canvas.current
    let ctx = cvs.getContext('2d')


    this.props.xValues.forEach((xValue, i) =>
    {
      let yValue = this.props.yValues[i]
      let xDelta = this.props.maxXValue - this.props.minXValue
      let xRatio = (xValue - this.props.minXValue) / xDelta
      let x = cvs.width * xRatio
      let yDelta = this.props.maxYValue - this.props.minYValue
      let yRatio = 1 - (yValue - this.props.minYValue) / yDelta
      let y = cvs.height * yRatio

      if(i === 0)
      {
        ctx.moveTo(x, y)
        ctx.strokeStyle = this.props.color
        ctx.beginPath()
      }

      ctx.lineTo(x, y)
    })

    ctx.stroke()
    ctx.closePath()
  }

  render = () =>
    <div className={this.className}>
      <canvas
        ref={this.canvas}
        width={this.props.containerWidth}
        height={this.props.containerHeight}
        />
    </div>

  static defaultProps =
  {
    className: '',
    xValues: [],
    yValues: [],
    minXValue: 0,
    maxXValue: 0,
    minYValue: 0,
    maxYValue: 0,
    color: "white"
  }
})
