import React, {Component} from 'react'
import './index.css'

export default class Header extends Component
{
  render = () =>
    <div className='ui-header'>
      US National Housing Price Index
    </div>
}
