
import React, {Component} from 'react'
import './index.css'

export default class GraphLabel extends Component
{

  className =
    `ui-graph-label ${this.props.vertical ? ' vertical' : ''} ${this.props.className}`

  render = () =>
    <div className={this.className}>
      {this.props.value}
    </div>

  static defaultProps =
  {
    className: '',
    value: '',
    vertical: false
  }
}
