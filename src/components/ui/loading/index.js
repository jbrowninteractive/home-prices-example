import React, {Component} from 'react'
import './index.css'

export default class Loading extends Component
{
  render = () =>
    <div className='ui-loading'>
      Loading...
    </div>
}
