
import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import state from './state'

export default
{
    create(reducers)
    {
        return createStore( reducers, state, compose( applyMiddleware(thunk)) )
    }
}
