
let fs = require("fs")
let jsdom = require("jsdom")
let data = fs.readFileSync("data.html", "utf8")
let document = new jsdom.JSDOM(data).window.document
let table = document.getElementsByTagName("table")[0]
let rows = Array.prototype.slice.call(table.rows, 0)
let list = []

for(let row of rows)
{
    let index = Number(row.children[0].innerHTML)
    let year = Number(row.children[1].innerHTML)
    let population = Number(row.children[2].innerHTML)

    if(isNaN(year))
    {
        continue
    }

    list.push({
        index: index,
        year: year,
        population: population
    })
}

fs.writeFileSync("data.json", JSON.stringify(list))
